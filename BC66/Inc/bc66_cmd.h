#ifndef  __BC66_CMD_H
#define  __BC66_CMD_H

/*---------------------------------------------------------------------*/


#include "main.h"
#include "stdint.h"
#include "pin_config.h"

/*---------------------------------------------------------------------*/


/*typedef struct{
	uint8_t b;
}struct_Gdgdfgdf;

struct_Gdgdfgdf sTesst;
*/


#define RESET_PORT	RESET_BC66_GPIO_Port

/*----------------------------------------------------------------------*/
                            /* defination of macros */

/* defination macro that BC66 response AT command Fail */
#define BC66_RESP_FAIL 0

/* defination macro that BC66 response AT command Success*/
#define BC66_RESP_OK 1UL

/* defination macro that indicate status of flag */
#define ON 1UL

/* defination macro that indicate staus of flag */
#define OFF 0UL

/* defination macro that indicate that BC66 response is match with compared buffer */
#define MATCH 1UL

/* defination macro that indicate that BC66 response is not match with compared buffer */
#define NOT_MATCH 0UL

/* defination macro of uart response process checking cycle */
#define UART_CHECK_CYCLE 20UL

/* defination macro of maximum time that process allow to not reset the module */
#define MAX_FAIL_TIME 10UL

/* defination macro of time that status of RESET_PIN keep in active state to reset BC66 */
#define RESET_KEEP_TIME 75UL

/* defination macro of time that status of PWRKEY_PIN keep in active state to power on BC66 module */
#define POWER_ON_KEEP_TIME 700UL

#define TRUE 1

#define FALSE 0

#define AT_RESP_TIMEOUT  10000

#define STOP 0

#define COUNTINUE 1

/*----------------------------------------------------------------------------------------------------*/
/* define data type BC66 */
typedef struct
{
  uint8_t *data;
	uint16_t data_len;
	uint32_t time_res;
} bc66_data;

/* define AT command number */
typedef enum
{
	PWR_ON,
	SYN_BAUDRATE_BC66 ,
	CHECK_PIN_BC66,
	SET_NB_BAND_BC66,
	OFF_SLEEP_BC66,
	SAVE_NVRAM,
	DISPLAY_CELLS_INFOR_BC66,
	CHECK_SIM_ATTACH_BC66,
	CHECK_IP_ADDRESS,
	SET_PSD_CONECTION,
	FREE_LOCK_NB_FREQUENCY,
	ENABLE_WAKEUP_INDICATION,
} AT_cmd;

/* define struct of AT command include command and desired response and number place in set of AT (AT_cmd) */
typedef struct
{
	uint8_t cmd;
	bc66_data AT_req;
  bc66_data AT_res;	
} AT_struct;	

typedef struct
{
	uint8_t fail_time;
	uint8_t reset_flag;
} reset_struct;


/*----------------------------------------------------------------------*/
                         /* extern variables section*/    
												 
extern volatile uint32_t tick_count;
//extern volatile uint32_t  time_count_AT_resp;
extern uint8_t pointer_code;
//extern volatile uint8_t check_flag;
//extern uint8_t reset_flag;
//extern uint8_t rx_process_flag;
//extern volatile uint32_t time_mark;
extern uint8_t AT_resp_result;
extern reset_struct bc66_reset;
/*----------------------------------------------------------------------*/
                         /* declare function section */
												 
uint8_t Check_AT_Response ( uint8_t AT_command );
void Check_Reset ( void );
void Reset_BC66 ( void );
uint8_t Excute_AT_Cmd ( uint8_t AT_command );
void BC66_Power_On ( void );
void Initialize_BC66_AT_Commands_Handler ( uint8_t current_cmd , uint8_t next_cmd );
uint8_t Check_Time_Out ( uint32_t mark , uint32_t cycle );
/*----------------------------------------------------------------------*/
#endif