/**
  * @brief   This file was created to define all function to contact with BC66 module.
	*          User can easily apply or change to fit their application
	* @creator Nguyen Binh Duong _ R&D Staff _ Sao Viet company
	* @data    7/12/2021
*/
/*-----------------------------------------------------------------------------------------*/
                              /* include area */
#include "bc66_uart.h"
#include "bc66_cmd.h"
#include "string.h"
#include "cmsis_os.h"
#include "pin_config.h"
/*-----------------------------------------------------------------------------------------*/
                              /* variable decalaration area */

volatile uint32_t tick_count;
reset_struct bc66_reset;
uint8_t pointer_code = PWR_ON;
uint8_t AT_resp_result = NOT_MATCH;

/*-----------------------------------------------------------------------------------------*/
                             /* constant declaration area */

const AT_struct set_AT[] =
{
 { PWR_ON                    ,    {NULL}                                                       , {NULL}                                    }, 	
 { SYN_BAUDRATE_BC66         ,    { (uint8_t*)"AT\r" , 3 , 5000 }                              , { (uint8_t*)"OK" , 2 , NULL }             },
 { CHECK_PIN_BC66            ,    { (uint8_t*)"AT+CPIN?\r" , 9 , 5000 }                        , { (uint8_t*)"+CPIN: READY" , 12 , NULL }  },
 { SET_NB_BAND_BC66          ,    { (uint8_t*)"AT+QBAND=1,3\r" , 13 , 5000 }                   , { (uint8_t*)"OK" , 2 , NULL }             },
 { OFF_SLEEP_BC66            ,    { (uint8_t*)"AT+QSCLK=0\r" , 12 , 5000 }                     , { (uint8_t*)"OK" , 2 , NULL }             },
 { SAVE_NVRAM                ,    { (uint8_t*)"AT&W0\r" , 6 , 5000 }                           , { (uint8_t*)"OK" , 2 , NULL }             },
 { DISPLAY_CELLS_INFOR_BC66  ,    { (uint8_t*)"AT+QENG=0\r" , 10 , 5000 }                      , { (uint8_t*)"OK" , 2 , NULL }             }, 
 { CHECK_SIM_ATTACH_BC66     ,    { (uint8_t*)"AT+CGATT?\r" , 10 , 12000 }                     , { (uint8_t*)"+CGATT: 1" , 9 , NULL }      },
 { CHECK_IP_ADDRESS          ,    { (uint8_t*)"AT+QIPADDR\r" , 11 , 10000 }                    , { (uint8_t*)"OK" , 2 , NULL }             },
 { SET_PSD_CONECTION         ,    { (uint8_t*)"AT+QCGDEFCONT=\"IP\",\"nbiot\"\r" , 27 , 5000 } , { (uint8_t*)"OK" , 2 , NULL}              },
 { FREE_LOCK_NB_FREQUENCY    ,    { (uint8_t*)"AT+QLOCKF=0\r" , 12 , 5000}                     , { (uint8_t*)"OK" , 2 , NULL}              },
 { ENABLE_WAKEUP_INDICATION  ,    { (uint8_t*)"AT+QATWAKEUP=1\r" , 15 , 5000}                  , { (uint8_t*)"OK" , 2 , NULL}              },
};
	
/*-----------------------------------------------------------------------------------------*/
                             /* defination of functions */

/**
  * @brief   Excute AT command and return response that exactly as requested or not
  * @param   AT_command      name or number of AT command in set of AT commands ( set_AT[] )
  * @retval  BC66_RESP_FAIL  reponse AT command fail
             BC66_RESP_OK    response AT sucess
*/
uint8_t Excute_AT_Cmd ( uint8_t AT_command )
{
	uint32_t time_mark = 0;
	Send_Data_To_BC66 ( set_AT[AT_command].AT_req.data , set_AT[AT_command].AT_req.data_len );
	AT_resp_result = NOT_MATCH;
	time_mark = tick_count;
  do
	{
	  if ( Check_Time_Out ( time_mark , AT_RESP_TIMEOUT ) == TRUE )
		  break;
		osDelay (1);
	} while ( AT_resp_result != MATCH );
	if ( AT_resp_result == MATCH ) return BC66_RESP_OK;
	else 
	  return BC66_RESP_FAIL;
}

/**
  * @brief   Check time out of event
  * @param   mark:  time that start check time out
  *          cycle: time that event time out from mark time
  * @retval  TRUE:  time out happend
  *          FALSE: time out not happend
*/
uint8_t Check_Time_Out ( uint32_t mark , uint32_t cycle )
{
	uint32_t delta;
	delta = tick_count - mark;
	if ( delta >= cycle)
		return TRUE;
	else 
		return FALSE;
}

/**
  * @brief   Check AT resposne of AT command 
  * @param   AT_command  number or name of AT command in set of AT commands ( set_AT[] )
  * @retval  MATCH       response match with required result 
             NOT_MATCH   response not match with required result
*/
uint8_t Check_AT_Response ( uint8_t AT_command )
{
	if ( strstr ( (char*)bc66_receive.rx_buff , (char*)set_AT[AT_command].AT_res.data ) != NULL )
		return MATCH;
	else
		return NOT_MATCH;
}

/**
  * @brief   Check process to determine need to reset or not
  * @param   NULL
  * @retval  NULL
*/
void Check_Reset ( void )
{
	switch ( bc66_reset.reset_flag )
	{
		case OFF:
			if ( bc66_reset.fail_time == MAX_FAIL_TIME )
			{
				Clear_BC66_Data ();
				bc66_reset.fail_time = 0;
				pointer_code = SYN_BAUDRATE_BC66;
				Reset_BC66();
			}
			break;
		case ON:
			Clear_BC66_Data ();
		  bc66_reset.fail_time = 0;
      bc66_reset.reset_flag = OFF;
      pointer_code = SYN_BAUDRATE_BC66;
      Reset_BC66();
			break;
	}
}

/**
  * @brief   This function was created to reset BC66
  * @param   NULL
  * @retval  NULL
*/
void Reset_BC66 ( void )
{
/*--------------------------------------------------------*/
	BC66_RESET_DOWN;
	HAL_Delay ( RESET_KEEP_TIME );
	BC66_RESET_UP;
	HAL_Delay ( 2000 );
/*--------------------------------------------------------*/
}

/**
  * @brief   This function was created to power on module BC66
  * @param   NULL
  * @retval  NULL
*/
void BC66_Power_On ( void )
{
	if ( pointer_code == PWR_ON )
	{
/*----------------------------------------------------------*/
	 BC66_PWR_KEY_DOWN;
	 HAL_Delay ( POWER_ON_KEEP_TIME );
	 BC66_PWR_KEY_UP;
/*----------------------------------------------------------*/
	 Reset_BC66 ();
	 HAL_Delay ( 2000 );
/*----------------------------------------------------------*/
	 pointer_code = SYN_BAUDRATE_BC66;
	}
}

/**
  * @brief   This function was created to handle AT commands in initialize BC66 section
  * @param   current_cmd: current AT command
  * @param   next_cmd: next AT command
  * @retval  NULL
*/

void Initialize_BC66_AT_Commands_Handler ( uint8_t current_cmd , uint8_t next_cmd )
{
  switch ( Excute_AT_Cmd ( current_cmd ) )
  {
    case BC66_RESP_OK:
			pointer_code = next_cmd;
		  bc66_reset.fail_time = 0;
			break;
		case BC66_RESP_FAIL:
			bc66_reset.fail_time++;
			break;
		default:
			Send_Data_To_Terminal ( ( uint8_t* )"nhay vao case khac\r\n" );
		break;
	}
}

		