/**
  * @brief    This file was created to initialize all function that use uart to contact with BC66
	            User can easily apply or change to fit their application
	* @creator 	Nguyen Binh Duong _ R&D Staff _ Sao Viet company
  * @data     7/12/2021	
*/
/*----------------------------------------------------------------------------------------------*/

#include "bc66_uart.h"
#include "string.h"
#include "bc66_cmd.h"
/*---------------------------------------------------------------------------------------------*/

/* declare variables area */

/* declare bc66 uart receive variable */
volatile uart_receive  bc66_receive;    

/* declare string that excute on screen when over flow happen*/
uint8_t string_over_flow[] ="Tran bo dem BC66 receive\r\n";


/*---------------------------------------------------------------------------------------------*/


/** 
  * @brief This function was excuted from HAL library 
	         and it excuted UART receive interrupt service rountine 
					 every time MCU complete receive 1 charater 
	* @note Read HAL UART driver for more detail
*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);

  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_UART_RxCpltCallback can be implemented in the user file.
   */
	if (huart->Instance == uart_bc66.Instance)
	{
		*( bc66_receive.rx_pointer ) = bc66_receive.rx_data;
		bc66_receive.rx_pointer++;
		bc66_receive.rx_byte++;
		if ( bc66_receive.rx_byte ==  MAX_ELEMENT_RECEIVE )
		{
			bc66_receive.rx_over_flow_flag = 1; 
		}
			HAL_UART_Receive_IT ( &uart_bc66 , &( bc66_receive.rx_data ) , 1 ); //enable receive interrupt
	}
}

/**
  * @brief  Init receive BC66 uart interrupt and initialize element in bc66_receive struct
  * @param  NULL
  * @retval NULL
*/
void BC66_UART_Receive_Init (void)
{
	bc66_receive.rx_pointer = bc66_receive.rx_buff;// pointer of bc66_receive point to bc66_receive buffer
	bc66_receive.rx_byte = 0;    //byte receive from BC66_uart
	bc66_receive.rx_data = 0; // initialize bc66_receive rx_data
	bc66_receive.pre_rx_byte = 0; // initialize bc66 pre receive byte
	bc66_receive.rx_over_flow_flag = 0; // initialize bc66 receive over flow flag
	HAL_UART_Receive_IT ( &uart_bc66 , &(bc66_receive. rx_data) , 1 ); //enable receive interrupt
}

/**
  * @brief  This is function was created to transmit data from MCU to BC66
  * @param  uint8_t *Data: pointer point to location that contain data need to be transmited
  * @retval NULL
*/
void Send_Data_To_BC66 ( uint8_t *data , uint16_t data_len )
{
	HAL_UART_Transmit ( &uart_bc66 , data , data_len , HAL_MAX_DELAY );
}

/**
  * @brief   This function was created to transmit data from MCU to PC terminal through UART
  * @param   uint8_t *Data: pointer point to location that contain data need to be transmited
  * @retval  NULL
*/
void Send_Data_To_Terminal ( uint8_t *Data )
{
	HAL_UART_Transmit ( &uart_debug , Data , strlen( (char*)Data ) , HAL_MAX_DELAY );
}

/**
  * @brief  Check receive process status
  * @param  NULL
  * @retval DONE
            NOT_DONE
*/
uint8_t Check_Receive_Process_Status (void)
{
	if ( bc66_receive.rx_byte != 0 )
	{
		if ( bc66_receive.rx_byte == bc66_receive.pre_rx_byte )
			return DONE;
		else if ( bc66_receive.rx_byte != bc66_receive.pre_rx_byte )
		{
		  bc66_receive.pre_rx_byte = bc66_receive.rx_byte;
			return NOT_DONE;
		}	
	}	
	else if (bc66_receive.rx_byte == 0)
	{
		return NONE;
	}
}

/**
  * @brief   Reset buffer that was pointed to
  * @param   uint8_t *buffer: Pointer point to buffer that need to reset
  * @retval  NULL
*/
void Reset_Buffer ( uint8_t *buffer )
{
  memset ( buffer , 0x00 , strlen ( (char*)buffer ) );
}

/**
  * @brief   Clear data in receive from BC66 and recieve process status
  * @param   NULL
  * @retval  NULL
*/
void Clear_BC66_Data ( void )
{
	Reset_Buffer ( ( uint8_t* )bc66_receive.rx_buff );
  bc66_receive.rx_byte = 0;
  bc66_receive.pre_rx_byte = 0;  
	bc66_receive.rx_pointer = bc66_receive.rx_buff;
	bc66_receive.rx_data = 0;
}