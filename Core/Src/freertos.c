/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "bc66_cmd.h"
#include "bc66_uart.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern AT_struct set_AT[];
/* USER CODE END Variables */
osThreadId NB_BC66Handle;
osThreadId Rx_HandlerHandle;
osSemaphoreId rx_semaHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void NB_BC66_Task(void const * argument);
void Rx_Handler_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of rx_sema */
  osSemaphoreDef(rx_sema);
  rx_semaHandle = osSemaphoreCreate(osSemaphore(rx_sema), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of NB_BC66 */
  osThreadDef(NB_BC66, NB_BC66_Task, osPriorityNormal, 0, 128);
  NB_BC66Handle = osThreadCreate(osThread(NB_BC66), NULL);

  /* definition and creation of Rx_Handler */
  osThreadDef(Rx_Handler, Rx_Handler_Task, osPriorityNormal, 0, 128);
  Rx_HandlerHandle = osThreadCreate(osThread(Rx_Handler), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_NB_BC66_Task */
/**
  * @brief  Function implementing the NB_BC66 thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_NB_BC66_Task */
void NB_BC66_Task(void const * argument)
{
  /* USER CODE BEGIN NB_BC66_Task */
  /* Infinite loop */
  for(;;)
  {
		BC66_Power_On ();
		Check_Reset();
		switch ( pointer_code )
		{
			case SYN_BAUDRATE_BC66:
				osDelay ( 10000 );
			  Initialize_BC66_AT_Commands_Handler ( SYN_BAUDRATE_BC66 , CHECK_PIN_BC66 );
				break;
			case CHECK_PIN_BC66:
				Initialize_BC66_AT_Commands_Handler ( CHECK_PIN_BC66 , SET_NB_BAND_BC66 );
			  break;
			case SET_NB_BAND_BC66:
				Initialize_BC66_AT_Commands_Handler ( SET_NB_BAND_BC66 , OFF_SLEEP_BC66 );
				break;
			case OFF_SLEEP_BC66:
				Initialize_BC66_AT_Commands_Handler ( OFF_SLEEP_BC66 , CHECK_SIM_ATTACH_BC66 );
				break;
			case CHECK_SIM_ATTACH_BC66:
				Initialize_BC66_AT_Commands_Handler ( CHECK_SIM_ATTACH_BC66 , SET_PSD_CONECTION );
			  break;
			case SET_PSD_CONECTION:
				Initialize_BC66_AT_Commands_Handler ( SET_PSD_CONECTION , FREE_LOCK_NB_FREQUENCY );
			  break;
			case FREE_LOCK_NB_FREQUENCY:
				Initialize_BC66_AT_Commands_Handler ( FREE_LOCK_NB_FREQUENCY , ENABLE_WAKEUP_INDICATION );
			  break;
			case ENABLE_WAKEUP_INDICATION:
        Initialize_BC66_AT_Commands_Handler ( ENABLE_WAKEUP_INDICATION , SAVE_NVRAM );
			  break;
			case SAVE_NVRAM:
				if ( Excute_AT_Cmd ( SAVE_NVRAM ) == SAVE_NVRAM )
					pointer_code = SAVE_NVRAM;
				else 
					bc66_reset.fail_time++;
			  break;
		}
  }
  /* USER CODE END NB_BC66_Task */
}

/* USER CODE BEGIN Header_Rx_Handler_Task */
/**
* @brief Function implementing the Rx_Handler thread.
* @param argument: Not used
* @retval None
*/
uint8_t den_bao_hieu;
/* USER CODE END Header_Rx_Handler_Task */
void Rx_Handler_Task ( void const * argument )
{
  /* USER CODE BEGIN Rx_Handler_Task */
  uint32_t uart_mark_time = 0;
	uart_mark_time = tick_count;
  /* Infinite loop */
  for(;;)
  {
		if ( Check_Time_Out ( uart_mark_time , UART_CHECK_CYCLE ) == TRUE )
		{
			uart_mark_time = tick_count;
			AT_resp_result = NOT_MATCH;
		  if ( Check_Receive_Process_Status() == DONE )
		  {
		/*----------------------------------------------*/
			// kiem tra va thuc hien cac tac vu URC gui ve
	  /*----------------------------------------------*/
			  AT_resp_result = Check_AT_Response ( pointer_code );
			  Send_Data_To_Terminal ( ( uint8_t* ) bc66_receive.rx_buff );
			  Clear_BC66_Data ();
	//			den_bao_hieu = COUNTINUE;
		  }	
		}
  }
  /* USER CODE END Rx_Handler_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
